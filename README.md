# AngularJS / Cubenado Demo

This package contains an angular application with a custom <cubenado> directive. After installation you can run `gulp serve` to start the localhost web server. You can then navigation to http://localhost:5000 to view the application.

## Requirements
NodeJS
NPM
Gulp v4.0

## Install

You can install this package with `npm`. You will need nodejs, npm, and gulp installed.

CD into the package directory

### gulp

```shell
npm uninstall gulp -g
npm install gulpjs/gulp-cli#4.0 -g
```

### npm install dependencies

```shell
npm install
```

### run the application
```shell
gulp serve
```