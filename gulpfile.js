var gulp = require('gulp'),
	browserify = require('browserify'),
	autoprefixer = require('gulp-autoprefixer'),
	sass = require('gulp-sass'),
	watch = require('gulp-watch'),
	buffer = require('vinyl-buffer'),
	source = require('vinyl-source-stream'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	clean = require('gulp-clean'),
	sourcemaps = require('gulp-sourcemaps'),
	debug = require('gulp-debug'),
	fs = require('fs'),
	Q = require('q');

var config = {
	srcPath: './src',
	startLocalhostServer: false,
	appMain: './src/assets/js/app.js',
	src: {
		assetPath: './src/assets'
	},
	build: {
		buildPath: './dist',
		assetPath: './dist/assets',
		srcPath: './dist/src',
		fileName: 'app.js'
	}
};

// Init
function init() {
	setupTasks();
}

// Setup tasks
function setupTasks() {

	var defaultTasks = ['js', 'sass', 'views', 'fonts', 'images', 'watch', 'serve'];
	
	// Determine which js tasks to run based on environment
	function js() {

		console.log("compile js");
		
		return browserify()
	    .add(config.appMain)
	    .bundle()
	    .on('error', function (error) { 
	    	console.log("ERROR");
	    	console.log(error);
	    })
	    .pipe(source(config.build.fileName))
	    .pipe(buffer())
	    .pipe(sourcemaps.init({loadMaps: true}))
	   	.pipe(sourcemaps.write('./'))
	   	.pipe(gulp.dest(config.build.assetPath + '/js'));

	}

	function jsWorkers() {
		console.log("js workers");
		gulp.src(config.src.assetPath + '/js/**/worker.js')
		.pipe(gulp.dest(config.build.assetPath + '/js'));
	}
 	
	function fonts() {
		gulp.src(config.src.assetPath + '/fonts/**/*')
		.pipe(gulp.dest(config.build.assetPath + '/fonts'));
	}

	function images() {
		gulp.src(config.src.assetPath + '/img/**/*')
		.pipe(gulp.dest(config.build.assetPath + '/img'));
	}

	// Compile sass
	function sassy() {
		console.log("compile sass");
		return gulp.src(config.src.assetPath + '/sass/app.scss')
		.pipe(sass({outputStyle: 'compressed', onError: function(e) { console.log(e); } }))
		.pipe(autoprefixer("last 2 versions", "> 1%", "ie 8"))
		.pipe(concat('app.css'))
		.pipe(gulp.dest(config.build.assetPath + '/css'));
	}

	// Watch files for changes
	function watcher() {
		
		// Javascript
		watch([
			config.src.assetPath + '/js/**/*.js'
		], function () {
			js();
		});

		// SASS
		watch([
			config.src.assetPath + '/sass/**/*.scss',
			'!' + config.src.assetPath + '/sass/_*.scss'
		], function () {
			sassy();
		});

		// Index File
		watch([
			config.srcPath + '/index.html',
			config.src.assetPath + '/views/**/*.html'
		], function () {
			views();
		});
	}

	// Copy files here
	function copy() {
		return views();
	}

	// Copy over the views
	function views() {
		gulp.src([
			config.srcPath + '/index.html'
		])
		.pipe(gulp.dest(config.build.buildPath));

		return gulp.src([
			config.src.assetPath + '/views/**/*.html'
		])
		.pipe(gulp.dest(config.build.assetPath + '/views'));
	}

	// Start the new build fresh by deleting the old build
	function cleanBuild() {
		var deferred = Q.defer();

		fs.stat(config.build.buildPath, function (err, res) {
			
			if (err)
				return deferred.resolve();

			gulp.src([config.build.buildPath])
	 		.pipe(clean({
	 			read: false
	 		}))
	 		.on("finish", function () {
	 			deferred.resolve();
	 		});

		});

		return deferred.promise;
	}

	// Spin up a local web server
	function serve() {

		// Setup a live reload server
		var embedlr = require('gulp-embedlr'),
		    refresh = require('gulp-livereload'),
		    lrserver = require('tiny-lr')(),
		    express = require('express'),
		    livereload = require('connect-livereload'),
		    livereloadport = 35729,
		    serverport = 5000;

		// Set up an express server (but not starting it yet)
		var server = express();
		// Add live reload
		server.use(livereload({port: livereloadport}));
		// Use our 'dist' folder as rootfolder
		server.use(express.static(config.build.buildPath));
		// Start webserver
		server.listen(serverport);
		// Start live reload
		lrserver.listen(livereloadport);

		server.get('*', function(request, response, next) {
			response.sendfile(config.build.buildPath + '/index.html');
		});

		console.log("starting webserver on port: " + serverport);

	}

	// Define all of our tasks
	gulp.task('js', js);
	gulp.task('js-workers', jsWorkers);
	gulp.task('views', views);
	gulp.task('clean', cleanBuild);
	gulp.task('serve', serve);
	gulp.task('watch', watcher);
	gulp.task('sass', sassy);
	gulp.task('fonts', fonts);
	gulp.task('images', images);

	// Run dev tasks
	gulp.task('default', gulp.series('clean', gulp.parallel(defaultTasks)));

}

init();