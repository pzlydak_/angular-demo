module.exports = function ($q) {

	var users = require('../../data/users.json');
	var currentUser = null;

	return {
		getByEmailAndPassword: function (email, password) {

			var deferred = $q.defer();
			var user = null;

			for (var i in users) {
				if (users[i].email === email && users[i].password === password) {
					user = users[i];
					break;
				}
			}

			if (user) {
				currentUser = user;
				deferred.resolve(user);
			} else {
				deferred.reject("invalid login attempt");
			}

			return deferred.promise;

		},
		getCurrentUser: function () {
			return currentUser;
		}
	}

}