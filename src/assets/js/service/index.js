var app = require('angular').module('verizonApp');

app.factory('AuthService', require('./auth.service.js'));
app.factory('UserService', require('./user.service.js'));