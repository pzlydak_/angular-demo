module.exports = function ($q, $rootScope, UserService) {

	var token = null;
	var loggedIn = false;
	var currentUser = null;

	function setToken(token) {
		localStorage.setItem('token', token);
	}

	function getToken() {
		return localStorage.getItem('token');
	}

	function isLoggedIn() {

		if (localStorage.getItem('token'))
			loggedIn = true;

		return loggedIn;
	}

	function login(user) {
		var deferred = $q.defer();

		if (currentUser)
			user = currentUser;

		UserService.getByEmailAndPassword(user.email, user.password)
		.then(function (user) {
			setToken(user.token);
			loggedIn = true;
			$rootScope.$broadcast('AuthService.login');
			deferred.resolve(user);
		}, function (err) {
			deferred.reject(err);
		});

		return deferred.promise;
	}

	function logout() {
		localStorage.removeItem('token');
		loggedIn = false;
		$rootScope.$broadcast('AuthService.logout');
	}

	return {
		login: login,
		logout: logout,
		isLoggedIn: isLoggedIn,
		getToken: getToken,
		setToken: setToken
	}

}