module.exports = function (self) {

	var positions;

	self.addEventListener('message', function(message) {
		
		var data = message.data;

		update(data.positions);

	});

	function updatePosition(position) {

		position.x = position.offsetX + (Math.cos(position.angle) * position.radius);
		position.angle += position.velocityX;

		self.postMessage({
			cmd: 'updatePosition',
			position: position
		});

	}

	function update(items) {

		positions = items;

		var iterations = Math.floor(items.length / 8);
		var leftover = items.length % 8;
		var i = 0;

		if (leftover > 0){
		    do {
		        updatePosition(items[i++]);
		    } while (--leftover > 0);
		}

		do {
		    updatePosition(items[i++]);
		    updatePosition(items[i++]);
		    updatePosition(items[i++]);
		    updatePosition(items[i++]);
		    updatePosition(items[i++]);
		    updatePosition(items[i++]);
		    updatePosition(items[i++]);
		    updatePosition(items[i++]);
		} while (--iterations > 0);

	}

}