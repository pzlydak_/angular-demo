var THREE = require('three');
var Cube = require('./cube');
// TODO: figure out how to offload the processing to a work or a set of workers
// var work = require('webworkify');
// var worker = work(require('./worker.js'))

var sceneConfig = {
	width: 600,
	height: 600,
	viewAngle: 28,
	aspect: 0,
	near: 1,
	far: 10000
};

sceneConfig.aspect = sceneConfig.width / sceneConfig.height;

var scene,
	renderer,
	camera,
	cubes,
	particleContainer,
	randomness = 1,
	nadoWidth = 50,
	nadoHeight = 125,
	step = 0,
	radiusStep = 0,
	minRadius = 1,
	maxRadius = 100,
	materials = new Array(20),
	maxVelocity = 0.05,
	minVelocity = 0.01,
	requestId;

var offsetX = nadoWidth * 0.25,
	offsetY = -nadoHeight * 0.25,
	radius = minRadius;

var defaultMaterials = Array(2);
defaultMaterials[0] = new THREE.MeshBasicMaterial({color: 0x333333});
defaultMaterials[1] = new THREE.MeshBasicMaterial({color: 0x151515});

function shuffle (array) {
	var newArray = array.slice();
	var i = 0
	, j = 0
	, temp = null;

	for (i = newArray.length - 1; i > 0; i -= 1) {
		j = Math.floor(Math.random() * (i + 1));
		temp = newArray[i];
		newArray[i] = newArray[j];
		newArray[j] = temp;
	}
	return newArray;
}

var Cubenado = {

	init: function (options) {

		var particleCount = options.particleCount || particleCount;
		randomness = options.randomness || randomness;

		this.createMaterials();
		this.setupScene(options);
		this.updateParticleCount(particleCount);
		this.startRender();
		
	},
	setupScene: function (options) {

		var container = document.querySelector('#' + options.containerId);

		camera = new THREE.PerspectiveCamera(
			sceneConfig.viewAngle,
			sceneConfig.aspect,
			sceneConfig.near,
			sceneConfig.far
		);

		scene = new THREE.Scene();
		scene.add(camera);
		camera.position.z = 300;

		renderer = new THREE.WebGLRenderer();
		renderer.setSize(sceneConfig.width, sceneConfig.height);

		container.appendChild(renderer.domElement);

	},
	createMaterials: function () {

		var numMaterials = 50;

		for (var i = 0; i < numMaterials; i++) {
			materials[i] = new THREE.MeshBasicMaterial({color: Math.random() * 0xffffff});
		}

	},
	getRandomMaterial: function () {
		return materials[Math.floor(Math.random() * materials.length)];
	},
	clear: function () {

		scene.remove(particleContainer);

		offsetX = 0;
		offsetY = -nadoHeight * 0.5;
		step = 0;
		radiusStep = 0;
		radius = minRadius;
		
		particleContainer = new THREE.Object3D();
		particleContainer.position.x = 0;
		particleContainer.position.y = 0;
		particleContainer.position.z = 0;

	},
	updateParticleCount: function (num) {

		cubes = new Array(num);
		
		Cubenado.clear();
		
		step = (nadoHeight / num);
		radiusStep = (nadoWidth / num);

		var iterations = Math.floor(num / 8);
		var leftover = cubes.length % 8;
		var i = 0;
		var geometry = new THREE.BoxGeometry(1, 1, 1);

		function createCube(i) {

			var material = defaultMaterials[Math.floor(Math.random() * defaultMaterials.length)];
			var cube = new Cube(geometry, material, {
				angle: i,
				radius: radius,
				rotationSpeed: 1,
				velocityX: 0.01,
				index: i
			});

			cube.position.x = offsetX + (Math.sin(i + 1) * radius);
			cube.position.y = offsetY;
			
			
			offsetY += step;
			radius += radiusStep;

			if (radius > maxRadius)
				radius = maxRadius;

			cubes[i] = cube;

			particleContainer.add(cube);

		}

		if (leftover > 0) {
		    do {
		        createCube(i++);
		    } while (--leftover > 0);
		}

		do {
		    createCube(i++);
		    createCube(i++);
		    createCube(i++);
		    createCube(i++);
		    createCube(i++);
		    createCube(i++);
		    createCube(i++);
		    createCube(i++);
		} while (--iterations > 0);

		scene.add(particleContainer);

		this.updateRandomness(randomness);

	},
	updateRandomness: function (num) {

		var perc = num / 100;
		var numCubes = Math.floor(perc * cubes.length);
		var randomCubes = shuffle(cubes);
		var radiusOffset = 10;

		for (var i = 0; i < randomCubes.length; i++) {
			
			var cube = randomCubes[i];
			
			if (numCubes === 0) {
				cube.restoreDefaults();
			} else if (i < numCubes) {
				cube.setMaterial(this.getRandomMaterial());
				cube.velocityX = cube.originalValues.velocityX + (Math.random() * (maxVelocity - minVelocity) + minVelocity);
				cube.radius = cube.originalValues.radius + (Math.random() * (radiusOffset - 1) + 1);
			} else {
				cube.restoreDefaults();
			}

		}

		randomness = num;

	},
	updatePosition: function (cube) {

		cube.position.x = offsetX + (Math.cos(cube.angle) * cube.radius);
		cube.angle += cube.velocityX;

	},
	spin: function () {

		var iterations = Math.floor(cubes.length / 8);
		var leftover = cubes.length % 8;
		var i = 0;

		if (leftover > 0){
		    do {
		        Cubenado.updatePosition(cubes[i++]);
		    } while (--leftover > 0);
		}

		do {
		    Cubenado.updatePosition(cubes[i++]);
		    Cubenado.updatePosition(cubes[i++]);
		    Cubenado.updatePosition(cubes[i++]);
		    Cubenado.updatePosition(cubes[i++]);
		    Cubenado.updatePosition(cubes[i++]);
		    Cubenado.updatePosition(cubes[i++]);
		    Cubenado.updatePosition(cubes[i++]);
		    Cubenado.updatePosition(cubes[i++]);
		} while (--iterations > 0);

	},
	startRender: function () {
		this.render();
	},
	stopRender: function () {
		cancelAnimationFrame(requestId);
	},
	render: function () {
		requestId = requestAnimationFrame(Cubenado.render);
		Cubenado.spin();
		renderer.render(scene, camera);
	},
	destroy: function () {
		this.stopRender();
		this.clear();
	}

}

module.exports = Cubenado;
