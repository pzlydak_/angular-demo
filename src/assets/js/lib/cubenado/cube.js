var util = require('util');
var THREE = require('three');

function Cube(geometry, material, options) {

	var options = options || {};

	THREE.Mesh.call(this, geometry, material);

	this.rotationX = options.rotationX || 0.01;
	this.rotationY = options.rotationY || 0.01;
	this.rotationZ = options.rotationZ || 0.01;
	this.velocityX = options.velocityX || 0.01;
	this.radius = options.radius || 1;
	this.angle = options.angle || 1;
	this.index = options.index || 0;
	this.originalValues = {
		rotationX: this.rotationX,
		rotationY: this.rotationY,
		rotationZ: this.rotationZ,
		velocityX: this.velocityX,
		radius: this.radius,
		angle: this.angle,
		material: material
	};

}

util.inherits(Cube, THREE.Mesh);

Cube.prototype.setMaterial = function (material) {
	this.material = material;
}

Cube.prototype.restoreDefaults = function () {
	this.rotationX = this.originalValues.rotationX;
	this.rotationY = this.originalValues.rotationY;
	this.rotationZ = this.originalValues.rotationZ;
	this.material = this.originalValues.material;
	this.radius = this.originalValues.radius;
	this.velocityX = this.originalValues.velocityX;
}

module.exports = Cube;