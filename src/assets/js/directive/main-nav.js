module.exports = function () {
	
	return {
		restrict: 'E',
    	templateUrl: 'assets/views/directive/main-nav.html',
    	controller: function ($rootScope, $scope, AuthService) {

    		$scope.loggedIn = AuthService.isLoggedIn();

    		$rootScope.$on('AuthService.logout', function () {
    			$scope.loggedIn = AuthService.isLoggedIn();
    		});

    		$rootScope.$on('AuthService.login', function () {
    			$scope.loggedIn = AuthService.isLoggedIn();
    		});

    	}
	}

}