var Cubenado = require('../lib/cubenado');

module.exports = function ($timeout) {
	
	return {
		restrict: 'E',
    	templateUrl: 'assets/views/directive/cubenado.html',
    	scope: {
    		particleCount: "=",
    		randomness: "="
    	},
    	controller: function ($scope, $timeout) {

    		$scope.data = {
				particleCount: parseInt($scope.particleCount),
				randomness: parseInt($scope.randomness)
			};

    		Cubenado.init({
				containerId: 'cubenado',
				particleCount: $scope.data.particleCount,
				randomness: $scope.data.randomness
			});
			
			$scope.onParticleSliderChange = function () {
				Cubenado.updateParticleCount($scope.data.particleCount);
			}

			$scope.onRandomSliderChange = function () {
				Cubenado.updateRandomness($scope.data.randomness);
			}

			$scope.$on('$destroy', function () {
			    Cubenado.destroy();
			});

    	}
	}

}