module.exports = function () {
	
	return {
		restrict: 'E',
    	templateUrl: 'assets/views/directive/form-login.html',
    	scope: {},
    	controller: function ($scope, $state, AuthService) {

    		$scope.submitted = false;

    		$scope.user = {
    			email: null,
    			password: null
    		};

    		$scope.submit = function (isValid) {

    			$scope.submitted = true;
    			$scope.status = null;

    			if (isValid) {
    				AuthService.login($scope.user)
    				.then(function (data) {
    					
    					$state.go('dashboard');

    				}, function (err) {
    					
    					$scope.status = {
    						type: 'error',
    						message: err
    					}

    				});
    			}

    		}

    	}
	}

}