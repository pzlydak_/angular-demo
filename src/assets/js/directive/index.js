'use strict';

var app = require('angular').module('verizonApp');

app.directive('mainNav', require('./main-nav'));
app.directive('formLogin', require('./form-login'));
app.directive('dashboard', require('./dashboard'));
app.directive('cubenado', require('./cubenado'));