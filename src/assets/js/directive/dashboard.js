module.exports = function () {
	
	return {
		restrict: 'E',
    	templateUrl: 'assets/views/directive/dashboard.html',
    	scope: {},
    	controller: function ($rootScope, $scope, $state) {

    		$scope.data = {
    			section: 'dashboard'
    		};

            $scope.goto = function(state) {
                $scope.data.section = state;
                $state.go(state);
            }

    		$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {
    			$scope.data.section = toState.name;
    		});

            $scope.goto($state.current.name);

    	}
	}

}