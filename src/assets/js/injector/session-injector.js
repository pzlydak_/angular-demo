module.exports = function ($injector, AuthService) {

	var publicStates = ['home', 'login'];
	
    var sessionInjector = {
        request: function(config) {
            config.headers['x-session-token'] = AuthService.getToken();
            return config;
        }
    };
    return sessionInjector;
}