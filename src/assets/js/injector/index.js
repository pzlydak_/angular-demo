var app = require('angular').module('verizonApp');

app.factory('sessionInjector', require('./session-injector'));
app.factory('sessionRecoverer', require('./session-recoverer'));

app.config(['$httpProvider', function($httpProvider) {  
    $httpProvider.interceptors.push('sessionInjector');
    $httpProvider.interceptors.push('sessionRecoverer');
}]);