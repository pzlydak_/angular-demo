'use strict';
 
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var app = angular.module('verizonApp', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

	$locationProvider.html5Mode(true).hashPrefix('!');

	$urlRouterProvider.otherwise("/");

	$stateProvider
	.state('home', {
		url: "/",
		templateUrl: "assets/views/main/home.html",
		controller: function($scope) {

		}
	})
	$stateProvider
	.state('login', {
		url: "/login",
		templateUrl: "assets/views/main/login.html",
		controller: function($scope) {

		}
	})
	.state('logout', {
		url: "/logout",
		controller: function($scope, $state, AuthService) {
			AuthService.logout();
			$state.go('login');
		}
	})
	.state('dashboard', {
		url: "/dashboard",
		templateUrl: "assets/views/main/dashboard.html",
		controller: function($scope) {

		}
	})
	.state('dashboard.cubenado', {
		url: "/cubenado",
		templateUrl: "assets/views/main/dashboard.cubenado.html",
		controller: function($scope) {

		}
	});

})
.run(function ($rootScope, $state, AuthService) {

	var publicStates = ['home', 'login'];

	$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {
		if (publicStates.indexOf(toState.name) === -1) {
			if (!AuthService.isLoggedIn()) {
				event.preventDefault();
				$state.go('login');
			}
		} else {
			if (AuthService.isLoggedIn() && toState.name === "login") {
				event.preventDefault();
				$state.go('dashboard');
			}
		}
	})
	
});

require('./service');
require('./controller');
require('./directive');
require('./injector');